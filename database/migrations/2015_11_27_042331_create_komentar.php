<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKomentar extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('komentar', function (Blueprint $table){
			$table->integer('id_user')->unsigned();
			$table->foreign('id_user')->references('id')->on('users');

			$table->integer('id_event')->unsigned();
			$table->foreign('id_event')->references('id')->on('event');

			$table->text('komentar');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('komentar');
	}

}
