<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEvent extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('event', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('nama');
			$table->string('kategori');
			$table->string('subkategori');
			$table->text('deskripsi');
			$table->string('status');
			$table->dateTime('waktu_pembuatan');
			$table->dateTime('waktu_mulai');
			$table->dateTime('waktu_selesai');
			$table->string('alamat');
			$table->string('lng');
            $table->string('lat');
            $table->string('foto');
            $table->string('flag');
            $table->integer('id_user')->unsigned();
            $table->foreign('id_user')->references('id')->on('users');

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('event');
	}

}
