<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApproval extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('approval', function(Blueprint $table){
            $table->integer('id_reviewer')->unsigned();
            $table->foreign('id_reviewer')->references('id')->on('users');
            $table->integer('id_reviewee')->unsigned();
            $table->foreign('id_reviewee')->references('id')->on('users');

            $table->primary(['id_reviewer','id_reviewee']);

            $table->integer('approve');            

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('approval');
	}

}
