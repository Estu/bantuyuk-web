<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventUsers extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('event_users', function(Blueprint $table)
		{
			$table->integer('id_event')->unsigned();
			$table->foreign('id_event')->references('id')->on('event');
			
            $table->integer('id_user')->unsigned();
            $table->foreign('id_user')->references('id')->on('users');

            $table->primary(['id_event', 'id_user']);

            $table->string('rate');
            $table->string('komentar')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('event_users');
	}

}
