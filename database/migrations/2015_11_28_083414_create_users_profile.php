<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersProfile extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users_profile', function(Blueprint $table) {
			$table->increments('id');
			$table->interger('id_user')->unsigned();
			$table->foreign('id_user')->references('id')->on('users');
			$table->string('flag');
			$table->string('jenis');
			$table->string('foto');
			$table->date('tanggal_lahir');
			$table->text('deskripsi');
			$table->dateTime('last_login');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users_profile');
	}

}
