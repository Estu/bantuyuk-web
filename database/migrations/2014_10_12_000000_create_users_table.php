<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table) {
			$table->increments('id');
			$table->string('nama');
			$table->string('email')->unique();
			$table->string('password', 60);
			// $table->date('tanggal_lahir');
			// $table->dateTime('last_login');
			// $table->string('flag');
			// $table->string('jenis');
			// $table->text('deskripsi');
			// $table->string('foto');
			$table->rememberToken();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
